Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: padthv1
Upstream-Contact: Rui Nuno Capela <rncbc@rncbc.org>
Source: https://sourceforge.net/projects/padthv1/files/

Files: *
Copyright:
  2012-2025 rncbc aka Rui Nuno Capela
License: GPL-2+

Files: debian/*
Copyright:
 2017 Jaromír Mikeš <mira.mikes@seznam.cz>
 2020-2025 Dennis Braun <snd@debian.org>
License: GPL-2+

Files:
  src/appdata/org.rncbc.padthv1.metainfo.xml
Copyright:
 2003-2024 Rui Nuno Capela <rncbc@rncbc.org>
License: FSFAP

Files:
 src/lv2/lv2_external_ui.h
Copyright:
 Filipe Coelho <falktx@falktx.com>
License: public-domain

Files:
 src/lv2/lv2_programs.h
Copyright:
 2012 Filipe Coelho <falktx@falktx.com>
License: ISC

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: public-domain
 This work is in public domain.
 .
 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.
